/*
 * GccApplication1.c
 *
 * Created: 2018-06-28 03:43:43

 */ 


#include <avr/io.h>
#include <util/delay.h>
void pwm_init()
{
	// greitasis PWM, OC0 aukstas lygis, daliklis, prescale=1
	TCCR0 |= (1<<WGM00)|(1<<COM01)|(1<<WGM01)|(1<<CS00);
	
	// OC0 isvada nustatome kaip isvesties
	DDRB |= (1<<PB3);
}

void main()
{
	uint8_t brightness; // tai  8 bitu ilgio bezenklio sveikojo skaiciaus tipas
	
	
	pwm_init();
	
	
	while(1)
	{
		// padidinti zybejima
		for (brightness = 0; brightness < 255; ++brightness)
		{
			
			OCR0 = brightness;
			
			
			_delay_ms(50);
		}
		
		// mazinti zybejima
		for (brightness = 255; brightness > 0; --brightness)
		{
			
			OCR0 = brightness;
			
			
			_delay_ms(50);
		}
		
	}
}
